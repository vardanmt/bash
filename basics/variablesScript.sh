#! /bin/bash

varLaptop="Asus zenbook 14"
os=`uname -a`


echo "Second bash script with variables and params"
echo "My laptop name is $varLaptop"
echo "os info : $os"

echo "First user defined param is $1"
echo "Script name is $0"

fNum=1
sNum=2
summ=$((fNum+sNum))

echo "$fNum + $sNum = $summ"


host=`hostname`

ping -c 4 $host


echo "Done"
