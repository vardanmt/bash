#! /bin/bash

echo "WHILE LOOP"

full=true
counter=0

while [ $full == true ]; do
	echo "Inside while loop"
	if	[ $counter -gt  2  ]; then
		echo "counter is "$counter". "
		full=false
		echo "Inside if condition. Full is "$full" "
	fi
	counter=$(($counter+1))
done


echo "FOR LOOP"

for file in `ls -l *.txt`; do
 echo "File content is "$file""
done

for x in {1..3}; do
	echo "x = $x"
done
