#! /bin/bash

echo "Start if section"

if	[ "Tor" == "$1" ]; then
	echo "Hi $1. How are you?"
elif	[ "Marina" == "$1" ]; then
	echo "H $1. How are you?"
else	echo "Hello, What is your name?"
fi

echo "Start CASE section"

read -p "Please enter param: " number

case $number in
	1) echo "Provided mumber is one";;
	[2-9]) echo "Provided number is from two to nine";;
	"Tor") echo "Provided name is Tor";;
	*) echo "Don't know param, Please try another param."
esac
